from __future__ import unicode_literals

__title__ = 'Mayan EDMS'
__version__ = '3.1.7'
__build__ = 0x030107
__build_string__ = 'v3.1.7_Sun Oct 14 03:51:25 2018 -0400'
__author__ = 'Roberto Rosario'
__author_email__ = 'roberto.rosario@mayan-edms.com'
__description__ = 'Free Open Source Electronic Document Management System'
__license__ = 'Apache 2.0'
__copyright_short__ = '2011-2018 Roberto Rosario'
__copyright__ = '{} {}'.format('Copyright', __copyright_short__)
__website__ = 'https://www.mayan-edms.com'
